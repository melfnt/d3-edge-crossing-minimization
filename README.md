# d3-edge-crossing-minimization
Tool that computes the vertexes positions of a given graphs such that the number of edge crossings is minimized. Sooner it will be turned into a d3 plugin.

Try it here (only works on firefox for now): https://d3-edge-crossing-minimization.herokuapp.com/src/index.html

Les Miserables graph data are based on character coappearence in Victor Hugo's Les Misérables, compiled by [Donald Knuth](https://www-cs-faculty.stanford.edu/~knuth/sgb.html)
