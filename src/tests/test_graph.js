
var check_for_nans = function ( graph )
{
	let points = graph.nodes;
	ret = [];
	for ( let id in points )
	{
		let p=points[id];
		if ( isNaN (p.x) || isNaN (p.y) || isNaN (p.z) )
		{
			ret.push (id);
		}
	}
	return ret;
}

var check_for_nans_in_array = function ( V )
{
	let points = V;
	ret = [];
	for ( let id in points )
	{
		let p=points[id];
		if ( isNaN (p.x) || isNaN (p.y) || isNaN (p.z) )
		{
			ret.push (id);
		}
	}
	return ret;
}
