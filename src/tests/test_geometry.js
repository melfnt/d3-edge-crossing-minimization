
load ("../core/geometry.js");

var console = {
			log: function ()
			{
				var str = "";
				for (var i = 0; i < arguments.length; i++)
				{
					str = str + " " + arguments[i];
				}
				debug (str);
			}
		};


// best fitting plane: http://www.ilikebigbits.com/blog/2017/9/24/fitting-a-plane-to-noisy-points-in-3d
// plane from normal and point: https://en.wikipedia.org/wiki/Plane_(geometry)
// project a point on a plane: http://www.9math.com/book/projection-point-plane
// Line equation on a plane: https://math.stackexchange.com/questions/404440/what-is-the-equation-for-a-3d-line

var random_point_on_plane = function (a,b,c,d)
{
	var ret;
	if ( Math.random() < 0.333 )
	{
		let x = Math.random();
		let y = Math.random();
		let z = -(a*x+b*y+d) / c + Math.random()*_NOISE-_NOISE/2;
		ret = new Point3D (x,y,z);
	}
	else if ( Math.random() < 0.333 )
	{
		let x = Math.random();
		let z = Math.random();
		let y = -(a*x+c*z+d) / b + Math.random()*_NOISE-_NOISE/2;
		ret = new Point3D (x,y,z);		
	}
	else
	{
		let z = Math.random();
		let y = Math.random();
		let x = -(c*z+b*y+d) / a + Math.random()*_NOISE-_NOISE/2;;
		ret = new Point3D (x,y,z);		
	}
	return ret;
}

let true_a = Math.random ()*5 + 5;
let true_b = Math.random ()*5 + 5;
let true_c = Math.random ()*5 + 5;
let true_d = Math.random ()*5 + 5;

console.log ( "true parameters", true_a, true_b, true_c, true_d );

const _N=10;
const _NOISE=2;

let points = [];
for ( let i=0; i<_N; ++i )
{
	let p = random_point_on_plane ( true_a, true_b, true_c, true_d );
	//~ console.log ( "true error", (p.x*true_a + p.y*true_b + p.z*true_c + true_d) / Math.sqrt(true_a**2+true_b**2+true_c**2) );
	points.push (p);
	console.log ("x",p.x,"y",p.y,"z",p.z);
}

let bfp = plane_fit ( points );

console.log ( "interpolated parameters", bfp.a, bfp.b, bfp.c, bfp.d );
for ( let i=0; i<_N; ++i )
{
	let p = points[i];
	//~ console.log ( "interpolation error", ( p.x*bfp.a + p.y*bfp.b + p.z*bfp.c + bfp.d ) / Math.sqrt(bfp.a**2+bfp.b**2+bfp.c**2) );
	let q = bfp.projection_2d ( p );
	console.log ("projected:",q.x,q.y)
}



