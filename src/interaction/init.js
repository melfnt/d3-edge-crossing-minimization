
const _SCROLL_FACTOR = 10;
const _ZOOM_FACTOR = 1.1;

var animation_mode = false;
var timeout_id = 0;
var dirty = false;
var useful_fps = 0;
var started_time = 0;
var common_zoom = 1;
var is_2D = false;
var last_graph_loaded  = null;

var G = null;
var RS = null;

var force_directed_simulation = null;

var onload = function ()
{	
	let select_elm = document.getElementById ("select_graph");
	populate_select ( select_elm );
	select_elm.addEventListener ( "change", new_graph_selected );
	
	document.getElementById ("preprocess").addEventListener ( "click", do_preprocessing );
	document.getElementById ("postprocess").addEventListener ( "click", do_postprocessing );
	document.getElementById ("resize").addEventListener ( "click", do_normalization );
	document.getElementById ("pancake").addEventListener ( "click", do_pancakeization );
	
	document.getElementById ("test_heuristic").addEventListener ( "click", sampling_euristic_from_a_random_vertex );
	document.getElementById ("update_lengths").addEventListener ( "click", do_update_springs_lengths );
	document.getElementById ("move_vertices").addEventListener ( "click", move_vertices );
	document.getElementById ("animation").addEventListener ( "click", do_animation );
	
	document.getElementById ("move_vertices_force").addEventListener ( "click", move_vertices_force );
	document.getElementById ("animation_force").addEventListener ( "click", do_animation_force );
	
	let canvas_elm = document.getElementById("canvas");
	global_ctx = canvas_elm.getContext("2d");
	canvas_elm.addEventListener( "keydown", key_pressed );
	canvas_elm.addEventListener( "wheel", mouse_scrolled );
	
	var sliders = document.getElementsByClassName ("parameter_slider");
	var texts = document.getElementsByClassName ("parameter_text");
	for (var i=0; i<sliders.length; ++i)
	{
		sliders[i].addEventListener ( "change", slider_changed_value );
		texts[i].addEventListener ( "change", text_changed_value );
		var event = new Event('change');
		sliders[i].dispatchEvent(event);
	}
	
	load_graph ("data/big_beehive.json");
	
}

var slider_changed_value = function ()
{
	var par_name = this.id.substring (7);
	//~ console.log ("changing slider ",par_name);
	var text = document.getElementById ("text_"+par_name);
	
	text.value = this.value;
	parameters[par_name] = parseFloat (this.value);
	//~ console.log (parameters);
}

var text_changed_value = function ()
{
	var par_name = this.id.substring (5);
	//~ console.log ("changing text ",par_name);
	var slider = document.getElementById ("slider_"+par_name);
	
	let new_value = parseFloat (this.value);
	let min_allowed = parseFloat(slider.min);
	let max_allowed = parseFloat(slider.max);
	
	//~ console.log (par_name);
	//~ console.log ("min/new/max",min_allowed,new_value,max_allowed);
	
	if ( new_value > max_allowed )
	{
		new_value = max_allowed;
	}
	if ( new_value < min_allowed )
	{
		new_value = min_allowed;
	}
	
	//~ console.log ("min/new/max",min_allowed,new_value,max_allowed);
	
	this.value = new_value;
	slider.value = new_value;
	parameters[par_name] = new_value;
	//~ console.log (parameters);
}

var sampling_euristic_from_a_random_vertex = function ()
{
	var rand_id = random_obj_property (G.nodes);
	//~ console.log ( rand_id );
	
	var otic = testing_in_canvas;
	testing_in_canvas = testing_in_canvas | _TESTING_SAMPLE_HEURISTIC;
	
	clear_canvas ();
	G.draw_in_canvas ();
	
	dir = do_sampling ( rand_id );
	console.log ("vertex",rand_id,"direction",dir);
	
	draw_colored_circle ( G.nodes[rand_id], "rgb(0,255,0)" );
	
	testing_in_canvas = otic;
	
}

var move_vertices = function ()
{
	console.log ("moving vertices");	
	let now = performance.now();
	let n = G.do_montecarlo_epoch ();
	console.log ("time elapsed", performance.now()-now,"ms");
	do_normalization ();
	update_statistics ();
}

var change_eta_if_necessary = function ()
{
	if ( force_directed_simulation.requested_force_factor < 1 )
	{
		console.log ("requested force factor",force_directed_simulation.requested_force_factor)
		let new_eta = parameters.ETA_force * force_directed_simulation.requested_force_factor ;
		let eta_slider = document.getElementById ("slider_ETA_force");
		eta_slider.value = new_eta;
		let event = new Event('change');
		eta_slider.dispatchEvent(event);
	}
}

var move_vertices_force = function ()
{
	//~ console.log ("moving vertices force");	
	let now = performance.now();
	force_directed_simulation.do_epoch ();
	change_eta_if_necessary ();
	
	let n = G.get_number_of_crossings ();
	//~ console.log ("time elapsed", performance.now()-now,"ms");
	do_normalization();
	update_statistics ();
		
	//~ console.log (G.plane.a+"\t"+G.plane.b+"\t"+G.plane.c+"\t"+G.plane.d);
	//~ for ( let A in G.nodes )
	//~ {
		//~ let p = G.nodes[A];
		//~ console.log (p.x+"\t"+p.y+"\t"+p.z);
	//~ }
	
	//~ console.log (JSON.parse(JSON.stringify(G.get_nodes())));
	//~ console.log (JSON.parse(JSON.stringify(G.get_links())));
	//~ console.log ("any NaN(s):", check_for_nans(G));
	
	//~ let nodes_distance = pairwise_distances ( G.nodes );
	//~ nodes_distance.sort (function (a,b){ return a.distance-b.distance; });
	//~ for ( let i=0; i<Math.min(5,nodes_distance.length) ; ++i )
	//~ {
		//~ console.log (nodes_distance[i].a, "|", nodes_distance[i].b, nodes_distance[i].distance);
	//~ }
	
	console.log ();
	
	
}

var async_draw_graph = function ()
{
	if (dirty)
	{
		dirty = false;
		console.log ("drawing...");
		do_normalization ()
		update_statistics ();
		useful_fps++;
		let now = performance.now();
		if ( now - started_time >= 1000 )
		{
			let fps = 1000 * useful_fps / (now - started_time);
			useful_fps = 0;
			started_time = now;
			document.getElementById ("useful_fps").innerHTML = fps.toFixed(2);
		}
	}
	if ( animation_mode )
	{
		window.requestAnimationFrame(async_draw_graph);
	}
}

var do_animation = function ()
{
	animation_mode = !animation_mode;
	
	if (animation_mode)
	{
		this.innerText = "stop animation";
		timeout_id = window.requestAnimationFrame(async_draw_graph);
		started_time = performance.now();
		cycle_epoch ();
	}
	else
	{
		this.innerText = "start animation";
		window.cancelAnimationFrame( timeout_id );
	}

}

var do_animation_force = function ()
{
	animation_mode = !animation_mode;
	
	if (animation_mode)
	{
		this.innerText = "stop animation";
		timeout_id = window.requestAnimationFrame(async_draw_graph);
		started_time = performance.now();
		cycle_force_epoch ();
	}
	else
	{
		this.innerText = "start animation";
		window.cancelAnimationFrame( timeout_id );
	}

}

var cycle_epoch = function ()
{
	let i=0,n=1;
	while ( animation_mode && i<10 && n!=0 )
	{
		let now = performance.now();
		n = G.do_montecarlo_epoch ();
		dirty = true;
		++i;
		console.log ("time elapsed",performance.now()-now,"ms");
	}
	if ( animation_mode && n==0 )
	{
		force_stop_animation ();
	}
	else
	{
		setTimeout (cycle_force_epoch, 100);
		//~ setTimeout (cycle_epoch, 100);
	}
}

var cycle_force_epoch = function ()
{
	let i=0;
	while ( animation_mode && i<10 )
	{
		let now = performance.now();
		force_directed_simulation.do_epoch ();
		change_eta_if_necessary ();
		dirty = true;
		++i;
		console.log ("time elapsed",performance.now()-now,"ms");
	}
	if ( animation_mode && force_directed_simulation.reached_stopping_criterion () )
	{
		force_stop_animation ();
	}
	else
	{
		//~ setTimeout (cycle_epoch, 100);
		setTimeout (cycle_force_epoch, 100);
	}
}

var load_graph = function (filename)
{
	last_graph_loaded = filename;
	force_stop_animation ();
	//~ console.log ("loading graph",filename);
	d3.json(filename, function(error, graph) 
	{
		if (error) throw error;
		
		//~ console.log (graph);
		
		G = new Graph3D ( graph.nodes, graph.links );
		
		transformation_matrix = [1,0,0,1,0,0];
		do_normalization ();
		
		update_statistics ();
		
		force_directed_simulation = new ForceDirected3D ( G );
		
		// TODO: comment
		//~ do_preprocessing ();
		//~ console.log ("preprocessed");
		//~ do_postprocessing ();
		//~ console.log ("postprocessed");
		
	});
	
}

var force_stop_animation = function ()
{
	document.getElementById ("animation").innerText = "start animation";
	document.getElementById ("animation_force").innerText = "start animation";
	animation_mode = false;
}

var do_preprocessing = function ()
{
	var r = simplify ( G );
	G = r.graph;
	RS = r.stack;

	clear_canvas ();
	G.draw_in_canvas ();
	
	update_statistics ();
	
}

var do_normalization = function ()
{
	const _MARGIN = 0.01;
	let graph2d = G.project_on_a_2d_plane ();
	let rect = bounding_box ( graph2d.nodes );
	let zoom_x = (1-2*_MARGIN) / (rect.max.x - rect.min.x);
	let zoom_y = (1-2*_MARGIN) / (rect.max.y - rect.min.y);
	common_zoom = Math.min(zoom_x, zoom_y);
	let transl_x = -rect.min.x + _MARGIN*(rect.max.x - rect.min.x);
	let transl_y = -rect.min.y + _MARGIN*(rect.max.y - rect.min.y);
	let w = global_ctx.canvas.width;
	let h = global_ctx.canvas.height;
	global_ctx.setTransform (1,0,0,1,0,0);
	
	global_ctx.scale (common_zoom,common_zoom);
	//~ console.log (common_zoom.toFixed(2) + "x");
	document.getElementById ("zoom_level").innerHTML = common_zoom.toFixed(2) + "x";
	
	global_ctx.translate (transl_x*w, transl_y*h);
	transformation_matrix = global_ctx.mozCurrentTransform;
	clear_canvas ();
	G.draw_in_canvas ();
}

var do_pancakeization = function ()
{
	is_2D = !is_2D;
	if (is_2D)
	{
		let bfp = plane_fit ( G.nodes );
		for ( A in G.nodes )
		{
			let old_pos = G.nodes[A];
			let new_pos = bfp.projection_2d ( old_pos );
			G.nodes[A] = new Point3D ( new_pos.x, new_pos.y, 0 );
			G.invalidate_plane_cache ();
			do_normalization ();
			update_statistics ();
		}
		this.innerHTML = "restore";
	}
	else
	{
		load_graph (last_graph_loaded);
		this.innerHTML = "pancake";
	}
}

var do_postprocessing = function ()
{
	if ( RS!=null )
	{
		var r = {"graph":G, "stack":RS};
		G = restore_removed_parts (r);
		RS = null;
		
		clear_canvas ();
		G.draw_in_canvas ();

		update_statistics ();
	
	}
}

var update_statistics = function ( graph )
{
	if ( ! graph )
	{
		graph = G;
	}
	
	document.getElementById ("text_epochs_number").innerHTML = graph.epoch;
	document.getElementById ("text_crossings_number").innerHTML = graph.get_number_of_crossings ();
	document.getElementById ("text_nodes_number").innerHTML = graph.n;
	document.getElementById ("text_links_number").innerHTML = graph.m;
	
	let plane = graph.plane;
	if ( plane )
	{
		//~ console.log ("new plane created");
		//~ console.log ("u ->",plane.axis_u.to_string())
		//~ console.log ("v ->",plane.axis_v.to_string());

		document.getElementById ("bfp_a").innerHTML = plane.a.toFixed(4);
		document.getElementById ("bfp_b").innerHTML = plane.b.toFixed(4);
		document.getElementById ("bfp_c").innerHTML = plane.c.toFixed(4);
		document.getElementById ("bfp_d").innerHTML = plane.d.toFixed(4);
		document.getElementById ("bfp_error").innerHTML = graph.get_total_projection_error ().toFixed(4);
	}
	
	if (force_directed_simulation)
	{
		document.getElementById("total_force").innerHTML = force_directed_simulation.total_force.toFixed(8);
	}
	
}

var populate_select = function ( select )
{
	let c=1;
	for ( let fname in avaliable_graphs  )
	{
		let desc = avaliable_graphs[fname];
		let option = document.createElement ("option");
		option.innerHTML = c+". "+desc;
		option.value=fname;
		select.appendChild (option);
		c = c+1;
	}
}

var new_graph_selected = function ()
{
	load_graph ( this.value );
	document.getElementById("select_graph").selectedIndex = 0;
}

// TODO: cache
var do_update_springs_lengths = function ()
{
}

var key_pressed = function ( evt )
{
	var ikey = evt.key.toUpperCase ();
	switch (ikey)
	{
		case "W": 
		 global_ctx.translate (0,_SCROLL_FACTOR);
		 break;
		case "A": 
		 global_ctx.translate (_SCROLL_FACTOR,0);
		 break;
		case "S": 
		 global_ctx.translate (0,-_SCROLL_FACTOR);
		 break;
		case "D": 
		 global_ctx.translate (-_SCROLL_FACTOR,0);
		 break;
	}
	if ( ikey=="W" || ikey=="A" || ikey=="S" || ikey=="D" )
	{
		transformation_matrix = global_ctx.mozCurrentTransform;
		clear_canvas ();
		G.draw_in_canvas ();
	}
}

var mouse_scrolled = function ( evt )
{
	evt.preventDefault ();
	let zoom_level = _ZOOM_FACTOR;
	if ( evt.deltaY > 0 )
	{
		zoom_level = 1/zoom_level;
	}
	global_ctx.translate ( evt.offsetX, evt.offsetY );
	global_ctx.scale ( zoom_level, zoom_level );
	global_ctx.translate ( -evt.offsetX, -evt.offsetY );
	transformation_matrix = global_ctx.mozCurrentTransform;
	clear_canvas ();
	G.draw_in_canvas ();
}

window.onload = onload;

var avaliable_graphs = {"data/k4.json":"k4",
                        "data/k4_kite.json":"kite",
                        "data/k5.json":"k5",
                        "data/k33.json":"Water, gas and electricity",
                        "data/petersen.json":"Petersen graph",
                        "data/cube.json":"cube graph",
                        "data/star.json":"star",
                        "data/btree.json":"binary tree",
                        "data/small_square_grid.json":"small square grid",
                        "data/big_square_grid.json":"big square grid",
                        "data/rect_square_grid.json":"rectangular square grid",
                        "data/square_strip.json":"square strip",
                        "data/small_cgrid.json":"small continuous square grid",
                        "data/big_cgrid.json":"big continuous square grid",
                        "data/small_beehive.json":"small beehive",
                        "data/medium_beehive.json":"medium beehive",
                        "data/big_beehive.json":"big beehive",
                        "data/miserables.json":"miserables"
                        };
