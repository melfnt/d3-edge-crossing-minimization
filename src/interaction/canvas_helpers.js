
var global_ctx = null;
var testing_in_canvas = 0;
var transformation_matrix = [1,0,0,1,0,0];

const _TESTING_SAMPLE_HEURISTIC = 1;
const _NODE_RADIUS = 5;
	

var string_hash = function (s)
{
	var hash = 0;
    for (var i = 0; i < s.length; ++i) 
    {
        var c = s.charCodeAt(i);
        hash = ((hash<<5)-hash)+c;
        hash = hash & hash;
    }
    return hash;
}

var string_to_rgb_color = function (s)
{
	var r = string_hash (s+"red") % 256;
	var g = string_hash (s+"green") % 256;
	var b = string_hash (s+"blue") % 256;
	return "rgb(" + r + "," + g + "," + b + ")" ;
}

var draw_result_circle = function ( p, score, total )
{
	var ctx = global_ctx;
	var ratio = (score/total * 200);
	ctx.fillStyle = "rgb(255,"+ratio+","+ratio+")";
	//~ console.log ("score",score,"total",total);
	//~ console.log ("point",p.x,p.y, ratio, ctx.fillStyle);
	ctx.beginPath();
	ctx.arc ( p.x*ctx.canvas.width, p.y*ctx.canvas.height, 5, 0, 2*Math.PI );
	ctx.closePath();
	ctx.fill();
}

var draw_colored_circle = function ( p, color )
{
	var ctx = global_ctx;
	ctx.fillStyle = color;
	//~ console.log ("colored circle:", color);
	//~ console.log ("point",p.x,p.y);
	ctx.beginPath();
	ctx.arc ( p.x*ctx.canvas.width, p.y*ctx.canvas.height, 5, 0, 2*Math.PI );
	ctx.closePath();
	ctx.fill();
}

var draw_line_in_direction = function ( p, dir )
{
	var ctx = global_ctx;
	//~ console.log ("point",p,"chosen direction",dir);
	var w = ctx.canvas.width;
	var h = ctx.canvas.height;
	ctx.strokeStyle = "#0000FF";
	ctx.beginPath();
	ctx.moveTo (p.x*w, p.y*h);
	ctx.lineTo (p.x*w+dir.x*100, p.y*h+dir.y*100);
	ctx.closePath();
	ctx.stroke();
}

var clear_canvas = function ()
{
	global_ctx.canvas.width = global_ctx.canvas.width;
	let t = transformation_matrix;
	//~ console.log ("transformation matrix",t);
	global_ctx.setTransform (t[0], t[1], t[2], t[3], t[4], t[5]);
}

var draw_graph = function (graph)
{	
	var ctx = global_ctx;
	
	var w = ctx.canvas.width;
	var h = ctx.canvas.height;
	
	ctx.strokeStyle = "#000000";
	ctx.lineWidth = 1/common_zoom;
	for ( var a in graph.links )
	{
		var list = graph.links[a];
		var source = graph.nodes[a];
		for ( var b in list )
		{
			var target = graph.nodes[b];
			ctx.beginPath();
			ctx.moveTo(source.x*w, source.y*h);
			ctx.lineTo(target.x*w, target.y*h);
			ctx.stroke();
		}
	}
	
	for ( var id in graph.nodes )
	{
		var n = graph.nodes[id];
		var color = string_to_rgb_color (id);
		draw_colored_circle ( n, color );
	}
}
