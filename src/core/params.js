
var parameters = 
{
	// sampling parameters
	"sampled_points_per_ring": 20,
	"rings_number" : 5,
	"starting_radius" : 0.01,
	"weigth_smooth": 0.001,
	
	// optimization and stability parameters
	"ETA_montecarlo": 0.1,
	"ETA_force": 0.1,
	"max_translation_tollerance": 0.5,
	"max_force_tollerance": 2,
	
	// force directed parameters
	"natural_spring_length" : 0.1,
	"spring_stiffness" : 1,
	"repulsion_constant" : 0.01,
	"force_too_little_to_continue": 1e-7,
	  // coplanar force parameters
	  "coplanar_ETA": 0.01,
	  "neighbor_hops": 2,
	  // anisotropic forces
	  "anisotropic_coefficient":1
	
}
