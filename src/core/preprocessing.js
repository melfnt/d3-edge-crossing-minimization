
var RemovedStack = function ()
{
	
	this.stack = [];
	
	/**
	 * 
	 * Called when one or more links from node A to node B is removed from the graph.
	 * There were how_many copies of the link to be removed.
	 * 
	 * This function is useful to keep track of the removed self loops and multiple copies of the same link.
	 * 
	 * 
	 * */
	this.removed_link = function ( A, how_many, B )
	{
		if (how_many > 0)
		{
			let ob = {"kind": "link", "source":A, "target":B, "how_many":how_many};
			this.stack.push (ob);
		}
	}
	
	/**
	 * 
	 * called when a node A is removed from the graph because its only neighbour was node B.
	 * 
	 * This is useful to keep track of the graph semplification from this situation:
	 * 
	 * \
	 * -B----A
	 * / 
	 * 
	 * to this:
	 * 
	 * \
	 * -B
	 * / 
	 * 
	 * */
	this.removed_node_degree_one = function ( A, B )
	{
		var ob = {"kind": "degree_one", "source":A, "target":B};
		this.stack.push (ob);
	}
	
	/**
	 * 
	 * called when a node A is removed from the graph because its only two neighbours were B and C.
	 * 
	 * This is useful to keep track of the graph semplification from this situation:
	 * 
	 * \           /
	 * -B----A----C-
	 * /           \
	 * 
	 * to this:
	 * 
	 * \           /
	 * -B---------C-
	 * /           \
	 * 
	 * */
	this.removed_node_degree_two = function ( B, A, C )
	{
		var ob = {"kind": "degree_two", "start":B, "middle":A, "end":C};
		this.stack.push (ob);		
	}
	
	this.pop = function ()
	{
		return this.stack.pop();
	}
	
}

var simplify = function ( graph )
{
	let n = graph.n;
	let m = graph.m;
	
	let rs = new RemovedStack ();
	
	let adj_matrix = new Array();
	let ids_to_check = new Set();
	let mapping = {};
	let rev_mapping = {};
	
	//~ console.log ("simplifyig graph", graph);
	
	let i=0;
	for ( let id in graph.nodes )
	{
		let row = []
		ids_to_check.add (id);
		mapping[id] = i;
		rev_mapping[i] = id;
		
		for ( let j=0; j<n; ++j )
		{
			row.push (0);
		}
		adj_matrix.push (row);	
		i = i+1;
	}
	
	console.log ("mapping between ids and positions:",mapping);
	console.log ("                      rev mapping:",rev_mapping);
		
	for ( let a in graph.links )
	{
		let list = graph.links[a];
		let r = mapping[a];
		for ( let b in list )
		{
			//~ console.log ("parsing link",a," -> ",b);
			let c = mapping[b];
			//~ console.log ("r",r,"c",c);
				
			adj_matrix[r][c] += graph.links[a][b];
		}
	}
	
	//~ console.log ("adj_matrix");
	//~ console.table (adj_matrix);
	
	while ( ids_to_check.size > 0 )
	{
		let next_ids_to_check = new Set ();
		console.log ("new cycle");
		
		for ( let id of ids_to_check.keys() )
		{
			next_ids_to_check.delete ( id );
			let A = id;
			let A_pos = mapping[id];
			let B_pos = null;
			let C_pos = null;
			let how_many_connected = 0;
			let i = mapping[id];
			
			console.log ("checking",A);
			console.log ("A_pos",A_pos);
			
			
			// self loop deleting
			rs.removed_link ( A, A, adj_matrix[i][i] );
			adj_matrix[i][i] = 0
			
			for ( let j=0; j<n && how_many_connected<3; ++j )
			{
				if ( adj_matrix[i][j] != 0 || adj_matrix[j][i] != 0 )
				{
					how_many_connected += 1;
					if ( B_pos == null )
					{
						B_pos = j;
						C_pos = j;
					}
					else
					{
						C_pos = j;
					}
				}
			}
			
			console.log ("how_many_connected", how_many_connected);
			if ( how_many_connected == 1 )
			{
				let B = rev_mapping[B_pos];
				console.log ("has degree 1: removing");
				
				rs.removed_link ( A, adj_matrix[A_pos][B_pos], B );
				rs.removed_link ( B, adj_matrix[B_pos][A_pos], A );

				rs.removed_node_degree_one ( A, B );
				
				adj_matrix[B_pos][A_pos] = adj_matrix[A_pos][B_pos] = 0;
				
				console.log ("adding", B, "to be checked");
				next_ids_to_check.add (B);
			}
			if ( how_many_connected == 2 )
			{
				let B = rev_mapping[B_pos];
				let C = rev_mapping[C_pos];
				console.log ("has degree 2: removing");
				
				rs.removed_link ( A, adj_matrix[A_pos][B_pos], B );
				rs.removed_link ( B, adj_matrix[B_pos][A_pos], A );
				rs.removed_link ( A, adj_matrix[A_pos][C_pos], C );
				rs.removed_link ( C, adj_matrix[C_pos][A_pos], A );
				rs.removed_node_degree_two ( B, A, C );
				
				adj_matrix[A_pos][B_pos] = adj_matrix[B_pos][A_pos] = adj_matrix[A_pos][C_pos] = adj_matrix[C_pos][A_pos] = 0;
				adj_matrix[B_pos][C_pos] += 1;
				
				console.log ("adding", B, C, "to be checked");
				next_ids_to_check.add (B);
				next_ids_to_check.add (C);	
			}
			
			//~ console.log ("after checking node", id);
			//~ console.log ("adjacency matrix:");
			//~ console.dir (adj_matrix);
			
		}
		
		ids_to_check = next_ids_to_check;
		console.log ("end cycle:")
		console.log ("needs to be (re)checked:",ids_to_check)
	}
	
	//~ console.log ("\n adding nodes and links \n");
	
	let remaining_nodes = new Set ();
	let remaining_links = [];
	for ( let i=0; i<n; ++i )
	{
		let A = rev_mapping[i];
		for ( let j=i+1; j<n; ++j )
		{
			let B = rev_mapping[j];
			if (adj_matrix[i][j] + adj_matrix[j][i] > 0)
			{
				remaining_nodes.add (A);
				remaining_nodes.add (B);
				remaining_links.push ( {"source": A, "target": B} );
			}
			
			rs.removed_link ( A, adj_matrix[i][j], B );
			rs.removed_link ( B, adj_matrix[j][i], A );
			
			adj_matrix[i][j] = adj_matrix[j][i] = 0;
	
		}
	}
	
	console.log ("remaining nodes", remaining_nodes);
	console.log ("remaining links", remaining_links);
	
	remaining_nodes_array = [];
	for ( let n of remaining_nodes )
	{
		remaining_nodes_array.push ({"id":n});
	}

	//~ console.log ("remaining nodes array", remaining_nodes_array);
	
	let new_graph = new Graph (remaining_nodes_array, remaining_links);
	
	// restore positions, number of epochs of the input graph
	for ( let n in new_graph.nodes )
	{
		new_graph.nodes[n] = graph.nodes[n];
	}
	new_graph.epoch = graph.epoch;
	
	//~ console.log ("returning",{"preprocessed_graph":new_graph, "stack":rs});
	return {"graph":new_graph, "stack":rs};
	
}

var restore_removed_parts = function ( simplified_graph )
{
	let graph = simplified_graph.graph;
	let stack = simplified_graph.stack.stack;
	
	console.log ("desimpligying graph",graph,"stack",stack);
	
	graph.links = {};
	graph.m=0;
	
	while ( stack.length > 0 )
	{
	
		let removed_parts = stack.pop();
		
		console.log ("new cycle: removed_parts",removed_parts);
		
		switch (removed_parts.kind)
		{
			case "link":
			{
				console.log ("link(s)");
				let A = removed_parts.source;
				let B = removed_parts.target;
				let how_many = removed_parts.how_many;
				graph.add_links ( A, B, how_many );
				break;
			}
			case "degree_one":
			{
				console.log ("degree 1");
						
				let A = removed_parts.source;
				let B = removed_parts.target;
				let p = graph.nodes[B];
				
				let segments_not_concerning_p = [];
				for ( let source in graph.links )
				{
					for ( let target in graph.links[source] )
					{
						if ( source!=B && target!=B )
						{
							let p1 = new Point ( graph.nodes[source].x, graph.nodes[source].y );
							let p2 = new Point ( graph.nodes[target].x, graph.nodes[target].y );
							segments_not_concerning_p.push ( new Segment (p1, p2) );
						}
					}
				}
				
				console.log ("target",B);
				console.log ("p",p);
				console.log ("segments_not_concerning_p",segments_not_concerning_p);
				
				let a;
				let radius = 0.4;
				let added = false;
	
				do
				{
					a = random_point_close_to ( p, radius );
					let segment = new Segment ( p, a );
					
					console.log ("tried a",a);
					console.log ("segment",segment);
					
					console.log ("---------------");
					console.log ("---------------");
					console.log ("---------------");
					console.log ("compute_pairwise_crossings ([segment], segments_concerning_p)",compute_pairwise_crossings ([segment], segments_not_concerning_p));
					console.log ("---------------");
					console.log ("---------------");
					console.log ("---------------");

					if ( compute_pairwise_crossings ([segment], segments_not_concerning_p) == 0 )
					{
						added = true;
					}
					
					radius = radius/2;
				} // do-while
				while ( added==false );
				
				graph.nodes[A] = a;
				graph.n++;
				break;
			}
			case "degree_two":
			{
				console.log ("degree 2");
				let B = removed_parts.start;
				let A = removed_parts.middle;
				let C = removed_parts.end;
				
				console.assert ( graph.links[B][C] > 0, "ERROR while restoring a degree_two removed node: B=", B, "A=", A, "C=", C, "graph=", graph );
				
				graph.remove_links (B,C,1)
				
				let p1 = graph.nodes[B];
				let p2 = graph.nodes[C];
				let m = new Point ( (p1.x+p2.x)/2, (p1.y+p2.y)/2 ); 
				
				graph.nodes[ A ] = m;
				graph.n++;
				
				break;
			}
		} // switch
		
		console.log ("n",graph.n,"m",graph.m);
		
	} // while
	
	return graph;
	
}
