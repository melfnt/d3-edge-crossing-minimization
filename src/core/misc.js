
function obj_len ( object )
{
	var props = Object.keys(object);
	return props.length;
}

function random_obj_property (obj)
{
	var props = Object.keys(obj);
	var rand_prop = props [ props.length * Math.random() << 0 ];
	return rand_prop;
}
