
var Graph3D = function (nodes, links)
{

	this.nodes = {};
	this.links = {};
	this.epoch = 0;
	this.n = nodes.length;
	this.m = links.length;
	this.cached_2d_graph = null;
	this.cache_epoch = -1;
	this.plane = null;
	
	for ( var i in nodes )
	{
		this.nodes[nodes[i].id] = new Point3D (Math.random(), Math.random(), Math.random());
		//~ this.nodes[nodes[i].id] = new Point3D (Math.random(), Math.random(), 1);
		this.links[nodes[i].id] = {} ;
	}
	
	for ( var i in links )
	{
		source = links[i].source;
		target = links[i].target;
		if (! (target in this.links[source]) )
		{
			this.links[source][target] = 0 ;
		}
		
		this.links[source][target] ++ ;
	}
	
	this.invalidate_plane_cache = function ()
	{
		this.cache_epoch = -1;
	}
	
	this.project_on_a_2d_plane = function ()
	{
		if ( this.epoch != this.cache_epoch ) 
		{
			this.cache_epoch = this.epoch;
			
			this.plane = plane_fit ( this.nodes );
			//~ console.log("best fitting plane:",this.plane);
			let nodes_2d = {};
			for ( let A in this.nodes )
			{
				nodes_2d[A] =  this.plane.projection_2d (this.nodes[A]) ;
				//~ console.log ("point3d",this.nodes[A],"point2d", nodes_2d[A]);
			}
			this.cached_2d_graph = new Graph ([],[]);
			this.cached_2d_graph.nodes = nodes_2d;
			this.cached_2d_graph.links = this.links;
			
		}
		return this.cached_2d_graph;
	}
	
	this.draw_in_canvas = function ()
	{
		let graph = this.project_on_a_2d_plane ();
		draw_graph ( graph );
	}
	
	this.copy = function ()
	{
		let ret = new Graph3D ( [], [] );
		for ( let A in this.nodes )
		{
			ret.nodes[A] = this.nodes[A];
			ret.links[A] = {};
			for (let B in this.links[A])
			{
				ret.links[A][B] = this.links[A][B];
			}
		}
		ret.epoch = this.epoch;
		return ret;
	}
	
	this.get_number_of_crossings = function ()
	{
		let graph2d = this.project_on_a_2d_plane ();
		return graph2d.get_number_of_crossings ();
	}
	
	this.add_links = function ( A, B, how_many )
	{
		if (! (A in this.links) )
		{
			this.links[A] = {};
		}
		
		if (! (B in this.links[A]) )
		{
			this.links[A][B] = 0 ;
		}
		
		this.links[A][B] += how_many ;
		this.m += how_many;
	}
	
	this.remove_links = function ( A, B, how_many )
	{
		this.links[A][B] -= how_many;
		if ( this.links[A][B] == 0 )
		{
			delete this.links[A][B];
		}
		this.m -= how_many;
		
	}
	
	this.print_edges = function ()
	{
		for ( let A in this.links )
		{
			for ( let B in this.links[A] )
			{
				console.log (A,B, this.links[A][B]);
			}
			console.log()
		}
	}
	
	this.get_total_projection_error = function ()
	{
		cum_distance = 0;
		for ( let A in this.nodes )
		{
			let p = this.nodes[A];
			cum_distance += this.plane.distance (p);
		}
		return cum_distance;
	}
	
	this.get_nodes = function ()
	{
		ret = {};
		for ( A in this.nodes )
		{
			ret[A] = this.nodes[A];
			ret[A].id = A;
		}
		return ret;
	}
	
	this.get_links = function ()
	{
		ret = [];
		for ( A in this.links )
		{
			for ( B in this.links[A] )
			{
				let link = {"source":A, "target":B};
				ret.push(link);
			}
		}
		return ret;
	}
	
	//~ this.normalize_nodes_in_the_unit_square = function ()
	//~ {
		//~ const THRESHOLD = 0.05;
		//~ let rect = bounding_box ( this.nodes );
		//~ let ratio_x = ( 1-2*THRESHOLD ) / ( rect.max.x - rect.min.x );
		//~ let ratio_y = ( 1-2*THRESHOLD ) / ( rect.max.y - rect.min.y );	
		//~ for ( let A in this.nodes )
		//~ {
			//~ let n = this.nodes[A];
			//~ console.log ("n before",n.x, n.y);
			//~ n.x = THRESHOLD + (n.x-rect.min.x)*ratio_x;
			//~ n.y = THRESHOLD + (n.y-rect.min.y)*ratio_y;
			//~ console.log ("n after",n.x, n.y);
		//~ }
		
	//~ }
	
}

var graph_from_node_object = function ( nodes, links )
{
	let ret = new Graph3D ( nodes, links );
	let n=0;
	for ( let A in nodes )
	{
		ret.nodes[A] = new Point3D (nodes[A].x, nodes[A].y, nodes[A].z) ;
		n++;
	}
	ret.n=n;
	return ret;
}
