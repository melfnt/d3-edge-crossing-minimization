
function Point (x,y)
{
	this.x = x;
	this.y = y;
	
	this.copy = function ()
	{
		return new Point ( this.x, this.y );
	}
	
	this.integer_coordinates = function ()
	{
		return new Point ( Math.floor (this.x), Math.floor (this.y) )
	}
	
	this.add = function (dx, dy)
	{
		if ( dy === undefined )
		{
			dy = dx.y;
			dx = dx.x;
		}
		this.x += dx;
		this.y += dy;
		return this;
	}
	
	this.normalize = function ()
	{
		var norm_factor = Math.sqrt (  this.x**2 + this.y**2  );
		this.x = this.x / norm_factor;
		this.y = this.y / norm_factor;
		return this;
	}
	
	this.scale = function ( sx, sy )
	{
		if ( sy === undefined )
		{
			sy = sx;
		}
		this.x *= sx;
		this.y *= sy;
		return this;
	}
}

function distance_squared ( p1, p2 )
{
	return (p1.x-p2.x)**2 + (p1.y-p2.y)**2;
}

function euclidean_distance ( p1, p2 )
{
	return Math.sqrt (distance_squared (p1, p2 ));
}

function Point3D (x,y,z)
{
	this.x = x;
	this.y = y;
	this.z = z;
	
	this.copy = function ()
	{
		return new Point3D ( this.x, this.y, this.z );
	}
	
	this.to_string = function ()
	{
		return "x: "+this.x.toFixed(4)+", y:"+this.y.toFixed(4)+", z:"+this.z.toFixed(4);
	}
	
	this.add = function (dx, dy, dz)
	{
		if ( dy === undefined )
		{
			dy = dx.y;
			dz = dx.z;
			dx = dx.x;
		}
		this.x += dx;
		this.y += dy;
		this.z += dz;
		return this;
	}
	
	this.normalize = function ()
	{
		var norm_factor = Math.sqrt (  this.x**2 + this.y**2 + this.z**2 );
		this.x = this.x / norm_factor;
		this.y = this.y / norm_factor;
		this.z = this.z / norm_factor;
		return this;
	}
	
	this.scale = function ( sx, sy, sz )
	{
		if ( sy === undefined )
		{
			sy = sx;
			sz = sx;
		}
		this.x *= sx;
		this.y *= sy;
		this.z *= sz;
		return this;
	}
	
	this.dot = function ( other )
	{
		return this.x*other.x + this.y*other.y + this.z*other.z; 
	} 
}

function distance_squared3D ( p1, p2 )
{
	return (p1.x-p2.x)**2 + (p1.y-p2.y)**2 + (p1.z-p2.z)**2;
}

function euclidean_distance3D ( p1, p2 )
{
	return Math.sqrt (distance_squared3D (p1, p2));
}

/**
 * 
 * implements a line, storing m and q so that the line is represented by:
 *   y = mx + q   if m is not Infinity
 *   x = q        if m is infinity
 * 
 * */
function Line ( m, q )
{
	this.m = m;
	this.q = q;
	
	this.contains_point = function ( p )
	{
		var ret = false;
		if ( this.m == Infinity )
		{
			ret = ( p.x == this.q );
		}
		else
		{
			ret = ( this.m * p.x + this.q == p.y );
		}
		return ret;
	} 
	
}

function Segment ( start, end )
{
	this.start = start;
	this.end = end;
	this.line = find_line_from_two_points ( start, end );
	
	this.contains_point = function (p)
	{
		return this.line.contains_point (p) && p.x >= min ( this.start.x, this.end.x ) && p.x <= max ( this.start.x, this.end.x ) && p.y >= min ( this.start.y, this.end.y ) && p.y <= max ( this.start.y, this.end.y );
	}
}


function find_line_from_two_points ( p1, p2 )
{
	var m = Infinity;
	var q = p1.x;
	if ( p2.x != p1.x )
	{
		m = ( p2.y - p1.y ) / ( p2.x - p1.x );
		q = p2.y - m * p2.x;
	}
	return new Line ( m, q ); 
}


function line_intersection ( l1, l2 )
{
	var ret = null;
	if ( l1.m != l2.m )
	{
		var x,y;
		if ( l1.m == Infinity )
		{
			x = l1.q;
			y = l2.m * x + l2.q;
		}
		else if ( l2.m == Infinity )
		{
			x = l2.q;
			y = l1.m * x + l1.q;
		}
		else
		{
			x = ( l2.q - l1.q ) / ( l1.m - l2.m )
			y = l1.m * x + l1.q;
		}
		ret = new Point ( x, y );
	}
	return ret;
}

function segments_intersection ( s1, s2 )
{
	var T = line_intersection ( s1.line, s2.line );
	var A = s1.start, B = s1.end;
	var C = s2.start, D = s2.end;
	if (  T != null && ( Math.max( Math.min(A.x,B.x), Math.min(C.x,D.x) ) > T.x || T.x > Math.min( Math.max(A.x,B.x), Math.max(C.x,D.x) ) 
	                  || Math.max( Math.min(A.y,B.y), Math.min(C.y,D.y) ) > T.y || T.y > Math.min( Math.max(A.y,B.y), Math.max(C.y,D.y) ) )  )
	{
		T = null;
	}
	return T;
}

/**
 * 
 * Given two line segments s1,s2 and a radius eps:
 *  - returns null if s1,s2 do not intersect.
 *  - otherwise, let T be the intersection of s1 and s2:
 *     - returns null if the distance between T and one of the endpoints of s1 or s2 is smaller than eps (the intersection between s1 and s2 is an endpoint with numerical error at most eps).
 *     - returns T otherwise.
 * 
 * */
function strict_segments_intersection ( s1, s2, eps )
{
	var T = segments_intersection (s1, s2);
	eps = eps**2;
	if ( T!=null && (  distance_squared (T, s1.start)<eps || distance_squared(T,s1.end)<eps  
	                || distance_squared (T, s2.start)<eps || distance_squared(T,s2.end)<eps ) ) 
	{
		T = null;
	}
	return T;
}

/**
 * 
 * given two arrays of Segments L1,L2 computes the number of crossing between every possible couple of segments l1,l2 taken from L1 and L2 respectively.
 * If a segment s is both in L1 and in L2, the crossings between s and s is not considered.
 * 
 * */
var compute_pairwise_crossings = function ( L1, L2 )
{
	var n = 0;
	for ( var i=0; i<L1.length; ++i )
	{
		for ( var j=0; j<L2.length; ++j )
		{
			//~ console.log ("segment1:", segment_to_string (L1[i]) );
			//~ console.log ("segment2:", segment_to_string (L2[j]) );
			//~ console.log ("strict intersection:", strict_segments_intersection ( L1[i], L2[j], 1e-6 ) );
			if ( L1[i] != L2[j] && strict_segments_intersection ( L1[i], L2[j], 1e-6 ) != null )
			{
				n = n+1;
			}
		}
	}
	//~ console.log ("returning",n)
	return n;
}

/**
 * 
 * given a Point p and a radius eps, returns a random point located at distance exactly eps from p.
 * 
 * */
function random_point_close_to ( p, eps )
{
	var rx = Math.random() * 2*eps - eps;
	var x = p.x + rx;
	var coef = Math.random()-0.5 > 0 ? 1 : -1;
	var y = p.y + coef * Math.sqrt ( eps**2 - rx**2 );
	return new Point ( x, y );
}

function segment_to_string ( segment )
{
	return "(" + segment.start.x + "," + segment.start.y + ") --- (" + segment.end.x + "," + segment.end.y + ")";
}

var bounding_box = function ( points )
{
	let min_x=Infinity, max_x=-Infinity, min_y=Infinity, max_y=-Infinity;
	for ( let A in points )
	{
		let n = points[A]
		if ( n.x > max_x )	max_x = n.x;
		if ( n.x < min_x )	min_x = n.x;
		if ( n.y > max_y )	max_y = n.y;
		if ( n.y < min_y )	min_y = n.y;
	}
	return {"min":new Point (min_x, min_y), "max":new Point (max_x, max_y)};
}

var bounding_box3D = function ( points )
{
	let min_x=Infinity, max_x=-Infinity, min_y=Infinity, max_y=-Infinity, min_z=Infinity, max_z=-Infinity;
	for ( let A in points )
	{
		let n = points[A];
		if ( n.x > max_x )	max_x = n.x;
		if ( n.x < min_x )	min_x = n.x;
		if ( n.y > max_y )	max_y = n.y;
		if ( n.y < min_y )	min_y = n.y;
		if ( n.z > max_z )	max_z = n.z;
		if ( n.z < min_z )	min_z = n.z;
	}
	return {"min":new Point3D (min_x, min_y, min_z), "max":new Point3D (max_x, max_y, max_z)};
}

var cross_product = function ( a, b )
{
	return new Point3D (  a.y*b.z - a.z*b.y, a.z*b.x - a.x*b.z, a.x*b.y - a.y*b.x  );
}

var orthonormal_axes = function ( n )
{
	let first_versor = new Point3D ( 1,0,0 );
	if ( Math.abs (n.y) <= Math.abs(n.x) && Math.abs (n.y) <= Math.abs(n.z) )
	{
		first_versor = new Point3D ( 0,1,0 );
	}
	else if ( Math.abs (n.z) <= Math.abs(n.x) && Math.abs (n.z) <= Math.abs(n.y) )
	{
		first_versor = new Point3D ( 0,0,1 );		
	}
	let u = cross_product ( n, first_versor );
	let v = cross_product ( n, u );
	
	//~ console.log ("n:",n.to_string());
	//~ console.log ("u before normalization:",u.to_string());
	//~ console.log ("v before normalization:",v.to_string());
	
	u.normalize();
	v.normalize();
	return {u:u, v:v};
}

/**
 * 
 * Implements a plane, storing a,b,c,d s.t. the plane is characterized by the equation ax + by + cz + d = 0
 * 
 * */
var Plane = function ( point, normal )
{
	let a,b,c,d;
	this.a = a = normal.x;
	this.b = b = normal.y;
	this.c = c = normal.z;
	this.d = d = - ( a*point.x + b*point.y + c*point.z );
	
	this.origin = point.copy();
	this.normal = normal.copy ();
	
	let two_vectors = orthonormal_axes ( normal );
	
	this.axis_u = two_vectors.u;
	this.axis_v = two_vectors.v;
	
	let A = [ [ this.axis_u.x, this.axis_v.x, normal.x ],
			  [ this.axis_u.y, this.axis_v.y, normal.y ],
			  [ this.axis_u.z, this.axis_v.z, normal.z ] ];
	this.cached_inverse_projection_matrix = math.inv (A);
	
	this.solve_projection_system = function ( point )
	{
		let p = point.copy();
		p.add ( -this.origin.x, -this.origin.y, -this.origin.z );
		let b = [[p.x],[p.y],[p.z]];
		let unknowns = math.multiply (this.cached_inverse_projection_matrix, b);
		return {s:unknowns[0][0], t:unknowns[1][0], r:unknowns[2][0]};
	}

	this.projection_2d = function ( point )
	{
		let unknowns = this.solve_projection_system ( point );
		return new Point ( unknowns.s, unknowns.t );
	}
	
	this.projection_3d = function ( point )
	{
		let unknowns = this.solve_projection_system ( point );
		let ret = this.origin.copy ();
		
		let s=unknowns.s, t=unknowns.t, u=this.axis_u, v=this.axis_v;
		
		ret.add ( s*u.x+t*v.x, s*u.y+t*v.y, s*u.z+t*v.z)
		return ret;
	}
	
	this.distance = function ( p )
	{
		return Math.abs(this.a*p.x + this.b*p.y + this.c*p.z + this.d) / Math.sqrt (this.a**2 + this.b**2 + this.c**2);
	}
	
}

var plane_fit = function ( points )
{
	let n = 0;
    let centroid = new Point3D (0,0,0);
    for ( let id in points )
    {
		let p = points[id];
        centroid.add (p);
        n = n+1;
    }
    
    if (n<3)
    {
		return null;
	}
    
    centroid.scale (1/n);
	let offset = centroid.copy().scale(-1);
	
    // Calculate full 3x3 covariance matrix, excluding symmetries:
    let xx = 0; let xy = 0; let xz = 0;
    let yy = 0; let yz = 0; let zz = 0;

    for ( let id in points )
    {
		let p = points[id];
		let r = p.copy().add (offset);
        xx += r.x * r.x;
        xy += r.x * r.y;
        xz += r.x * r.z;
        yy += r.y * r.y;
        yz += r.y * r.z;
        zz += r.z * r.z;
    }

    xx *= 1/n ;
    xy *= 1/n ;
    xz *= 1/n ;
    yy *= 1/n ;
    yz *= 1/n ;
    zz *= 1/n ;

    let weighted_dir = new Point3D (0,0,0);
    
	let det_x = yy*zz - yz*yz;
	let axis_dir = new Point3D ( det_x, xz*yz - xy*zz, xy*yz - xz*yy );
	let weight = det_x * det_x;
	if ( weighted_dir.dot( axis_dir ) < 0.0 )  weight = -weight;
	weighted_dir.add (axis_dir.scale(weight));

	let det_y = xx*zz - xz*xz;
	axis_dir = new Point3D ( xz*yz - xy*zz, det_y, xy*xz - yz*xx );
	weight = det_y * det_y;
	if ( weighted_dir.dot( axis_dir ) < 0.0 )  weight = -weight;
	weighted_dir.add (axis_dir.scale(weight));
	
	let det_z = xx*yy - xy*xy;
	axis_dir = new Point3D ( xy*yz - xz*yy, xy*xz - yz*xx , det_z );
	weight = det_z * det_z;
	if ( weighted_dir.dot( axis_dir ) < 0.0 )  weight = -weight;
	weighted_dir.add (axis_dir.scale(weight));
	
	//~ console.log ("n before normalization:",weighted_dir.to_string())
	
    let normal = weighted_dir.normalize();
    return new Plane (centroid, normal);
   
}

var pairwise_distances = function ( points )
{
	let ret = [];
	for ( let A in points )
	{
		let p = points[A];
		for ( let B in points )
		{
			if (A>B)
			{
				let q = points[B];
				obj = {a:A, b:B, distance:distance_squared3D(p,q)};
				ret.push (obj);
			}
		}
	}
	return ret;
}
