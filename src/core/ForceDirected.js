
function ForceDirected ( initial_graph )
{
	
	this.graph = initial_graph;
	this.springs_lengths = {};
	this.total_force = 0;
	
	for ( let A in initial_graph.links )
	{
		this.springs_lengths[A] = {};
		for ( let B in initial_graph.links[A] )
		{
			this.springs_lengths[A][B] = parameters.natural_spring_length;
		}
	}
	
	this.do_epoch = function ()
	{
		let springs_forces = {};
		let electric_forces = {};
		for ( let A in this.graph.nodes )
		{
			springs_forces[A] = new Point (0,0);
			electric_forces[A] = new Point (0,0);
		}
		
		for ( let A in this.graph.nodes )
		{
			let p = this.graph.nodes[A];
			for ( let B in this.graph.nodes )
			{
				if ( A > B )
				{
					let q = this.graph.nodes[B];
					let den = euclidean_distance ( p,q ) ** 3;
					let forcex = (p.x-q.x) / den;
					let forcey = (p.y-q.y) / den;
					electric_forces[A].add ( forcex, forcey );
					electric_forces[B].add ( -forcex, -forcey );
				}
			}
		}
		
		for ( let A in this.springs_lengths )
		{
			let p = this.graph.nodes[A];
			for ( let B in this.springs_lengths[A] )
			{
				let q = this.graph.nodes[B];
				let d = euclidean_distance ( p, q );
				let forcex = (1-this.springs_lengths[A][B] / d) * ( q.x - p.x );
				let forcey = (1-this.springs_lengths[A][B] / d) * ( q.y - p.y );
				springs_forces[A].add ( forcex, forcey );
				springs_forces[B].add ( -forcex, -forcey );
			}
		}
		
		this.total_force = 0;
		for ( let A in this.graph.nodes )
		{
			let df = springs_forces[A].scale ( parameters.spring_stiffness ).add ( electric_forces[A].scale ( parameters.repulsion_constant ) );
			this.graph.nodes[A].add ( df.scale ( parameters.ETA_force ) );
			this.total_force += distance_squared ( df, {x:0, y:0} );
		}
		console.log ("total force:", this.total_force);
		
		return this.graph;
	}
	
	this.update_springs_lengths = function ( new_springs_lengths, alpha )
	{
		for ( let A in new_springs_lengths )
		{
			for ( let B in new_springs_lengths[A] )
			{
				this.springs_lengths[A][B] = alpha*new_springs_lengths[A][B] + (1-alpha)*this.springs_lengths[A][B];
			}
		}
	}
	
	this.reached_stopping_criterion = function ()
	{
		return this.total_force < parameters.force_too_little_to_continue;
	}
	
}
