
var Graph = function (nodes, links)
{

	this.nodes = {};
	this.links = {};
	this.NODE_RADIUS = 5;
	this.epoch = 0;
	this.n = nodes.length;
	this.m = links.length;
	
	for ( var i in nodes )
	{
		this.nodes[nodes[i].id] = new Point (Math.random(), Math.random());
		this.links[nodes[i].id] = {} ;
	}
	
	for ( var i in links )
	{
		source = links[i].source;
		target = links[i].target;
		if (! (target in this.links[source]) )
		{
			this.links[source][target] = 0 ;
		}
		
		this.links[source][target] ++ ;
	}
	
	this.draw_in_canvas = function ()
	{
		draw_graph (this);		
	}
	
	this.copy = function ()
	{
		let ret = new Graph ( [], [] );
		for ( let A in this.nodes )
		{
			ret.nodes[A] = this.nodes[A];
			ret.links[A] = {};
			for (let B in this.links[A])
			{
				ret.links[A][B] = this.links[A][B];
			}
		}
		ret.epoch = this.epoch;
		return ret;
	}
	
	this.do_montecarlo_epoch = function ()
	{
		console.log ("epoch",this.epoch);
		
		var new_nodes = {};
		
		for ( var id in this.nodes )
		{
			var dir = do_sampling ( id );
			//~ console.log ("id",id,"x",this.nodes[id].x,"y",this.nodes[id].y);
			//~ draw_line_in_direction ( this.nodes[id], dir );
			new_nodes[id] = new Point ( this.nodes[id].x + dir.x*parameters.ETA_montecarlo, this.nodes[id].y + dir.y*parameters.ETA_montecarlo ); 
			//~ console.log ("new position","x",new_nodes[id].x,"y",new_nodes[id].y);
		}
		
		this.nodes = new_nodes;
		
		let n = this.get_number_of_crossings ()
		console.log ("number of crossing:",n);
		
		this.epoch ++ ;
		
		return n;
	}
	
	this.get_number_of_crossings = function ()
	{
		var segments = [];
		for ( var a in this.links )
		{
			var list = this.links[a];
			var p1 = new Point ( this.nodes[a].x, this.nodes[a].y );
			for ( var b in list )
			{
				var p2 = new Point ( this.nodes[b].x, this.nodes[b].y );
				segments.push ( new Segment (p1, p2) );
			}
		}
		var n = compute_pairwise_crossings ( segments, segments )/2;
		return n;
	}
	
	this.add_links = function ( A, B, how_many )
	{
		if (! (A in this.links) )
		{
			this.links[A] = {};
		}
		
		if (! (B in this.links[A]) )
		{
			this.links[A][B] = 0 ;
		}
		
		this.links[A][B] += how_many ;
		this.m += how_many;
	}
	
	this.remove_links = function ( A, B, how_many )
	{
		this.links[A][B] -= how_many;
		if ( this.links[A][B] == 0 )
		{
			delete this.links[A][B];
		}
		this.m -= how_many;
		
	}
	
	this.print_edges = function ()
	{
		for ( let A in this.links )
		{
			for ( let B in this.links[A] )
			{
				console.log (A,B, this.links[A][B]);
			}
			console.log()
		}
	}
	
	this.normalize_nodes_in_the_unit_square = function ()
	{
		const THRESHOLD = 0.05;
		let rect = bounding_box ( this.nodes );
		let ratio_x = ( 1-2*THRESHOLD ) / ( rect.max.x - rect.min.x );
		let ratio_y = ( 1-2*THRESHOLD ) / ( rect.max.y - rect.min.y );
		//~ console.log("min_x",min_x,"max_x",max_x);
		//~ console.log("ratio_x",ratio_x);
		//~ console.log("min_y",min_y,"max_y",max_y);
		//~ console.log("ratio_y",ratio_y);
		
		for ( let A in this.nodes )
		{
			let n = this.nodes[A];
			console.log ("n before",n.x, n.y);
			n.x = THRESHOLD + (n.x-rect.min.x)*ratio_x;
			n.y = THRESHOLD + (n.y-rect.min.y)*ratio_y;
			console.log ("n after",n.x, n.y);
		}
		
	}
	
}

var do_sampling = function ( start_vertex_id )
{	
	var vertex = G.nodes[start_vertex_id];
	var other_segments = [];
	var node_segments = [];
	for ( var a in G.links )
	{
		var list = G.links[a];
		var p1 = new Point ( G.nodes[a].x, G.nodes[a].y );
		
		for ( var b in list )
		{
			var p2 = new Point ( G.nodes[b].x, G.nodes[b].y );	
			if ( a==start_vertex_id  )
			{
				node_segments.push ( new Segment (p1, p2) );
			}
			else if ( b==start_vertex_id )
			{
				node_segments.push ( new Segment (p2, p1) );
			}
			else
			{
				other_segments.push ( new Segment (p1, p2) );
			}
		}
	}
	//~ console.log ( "segments not involving",start_vertex_id,":",other_segments.length );
	//~ console.log ( other_segments );
	//~ console.log ( "segments involving",start_vertex_id,":",node_segments.length );
	//~ console.log ( node_segments );
	
	var n_old = compute_pairwise_crossings ( node_segments, other_segments );
	//~ console.log ("crossings number",n_old);
	
	var cum_direction = new Point ( 0, 0 );
	let theta = 2*Math.PI / parameters.sampled_points_per_ring;
	
	if ( n_old != 0 )
	{
		
		var radius = parameters.starting_radius;
		let phi = Math.random () * 2 * Math.PI;
		
		for ( var i=0; i<parameters.rings_number; ++i)
		{
			for ( var j=0; j<parameters.sampled_points_per_ring; ++j )
			{
				var rp = new Point ( vertex.x+radius*Math.cos(phi + j*theta), vertex.y+radius*Math.sin(phi + j*theta));
				for ( var k=0; k<node_segments.length; ++k )
				{
					//~ console.log ("i=",i,"j=",j,"k=",k,"node_segments[k]=",node_segments[k]);
					node_segments[k] = new Segment ( rp, node_segments[k].end );
				}
				var n = compute_pairwise_crossings ( node_segments, other_segments );
				//~ console.log ("random point",rp);
				//~ console.log ("crossings number",n);
				if ( testing_in_canvas & _TESTING_SAMPLE_HEURISTIC )
				{
					draw_result_circle ( rp, n, n_old );
				}
				
				var dir = new Point (rp.x-vertex.x,rp.y-vertex.y).normalize();
				var weigth = n_old/(n+parameters.weigth_smooth);
				cum_direction.add ( dir.x*weigth, dir.y*weigth );
			}
			radius = radius*2;
		}
		
		cum_direction.normalize ();
		if ( testing_in_canvas & _TESTING_SAMPLE_HEURISTIC )
		{
			draw_line_in_direction ( vertex, cum_direction );
		}
	}
	
	return cum_direction;
}
