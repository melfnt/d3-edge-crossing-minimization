
function ForceDirected3D ( initial_graph )
{
	
	this.graph = initial_graph;
	this.springs_lengths = {};
	this.total_force = 0;
	this.cached_neighs = {};
	this.requested_force_factor = 1;
	
	let undirected_links = {};
	for ( let A in initial_graph.nodes )
	{
		undirected_links[A] = {};
	}
	
		
	for ( let A in initial_graph.links )
	{
		this.springs_lengths[A] = {};
		for ( let B in initial_graph.links[A] )
		{
			this.springs_lengths[A][B] = parameters.natural_spring_length;
			undirected_links[A][B] = 1;
			undirected_links[B][A] = 1;
		}
		
	}
	
	for ( let A in initial_graph.links )
	{
		this.cached_neighs[A] = new Set ();
		let frontier = new Set ();
		frontier.add (A);
		for ( let i=0; i<parameters.neighbor_hops+1; ++i )
		{
			let new_frontier = new Set ();
			for ( let B of frontier.keys() )
			{
				this.cached_neighs[A].add(B);
				for ( C in undirected_links[B] )
				{
					new_frontier.add (C);
				}
			}
			frontier = new_frontier;
		} 
		//~ console.log ("cached_neighs of",A,this.cached_neighs[A]);
	}
	
	for ( let A in this.cached_neighs )
	{
		if ( this.cached_neighs[A].size < 3 )
		{
			delete (this.cached_neighs[A]);
		}
	}
		
	this.do_epoch = function ()
	{
		this.requested_force_factor = 1;
		
		this.graph.epoch ++;
		let springs_forces = {};
		let electric_forces = {};
		let coplanar_forces = {};
		for ( let A in this.graph.nodes )
		{
			springs_forces[A] = new Point3D (0,0,0);
			electric_forces[A] = new Point3D (0,0,0);
			coplanar_forces[A] = new Point3D (0,0,0);
		}
		
		for ( let A in this.graph.nodes )
		{
			let p = this.graph.nodes[A];
			for ( let B in this.graph.nodes )
			{
				if ( A > B )
				{
					let q = this.graph.nodes[B];
					let den = euclidean_distance3D ( p,q ) ** 2;
					let forcex = (p.x-q.x) / den;
					let forcey = (p.y-q.y) / den;
					let forcez = (p.z-q.z) / den;
					electric_forces[A].add ( forcex, forcey, forcez );
					electric_forces[B].add ( -forcex, -forcey, -forcez );
				}
			}
		}
		
		for ( let A in this.springs_lengths )
		{
			let p = this.graph.nodes[A];
			for ( let B in this.springs_lengths[A] )
			{
				let q = this.graph.nodes[B];
				let d = euclidean_distance3D ( p, q );
				let forcex = (1-this.springs_lengths[A][B] / d) * ( q.x - p.x );
				let forcey = (1-this.springs_lengths[A][B] / d) * ( q.y - p.y );
				let forcez = (1-this.springs_lengths[A][B] / d) * ( q.z - p.z );
				springs_forces[A].add ( forcex, forcey, forcez );
				springs_forces[B].add ( -forcex, -forcey, -forcez );
			}
		}
		
		for ( let A in this.cached_neighs )
		{
			let points = [];
			for ( let B of this.cached_neighs[A].keys() )
			{
				points.push (this.graph.nodes[B]);
			}
			
			//~ console.log ("points:",points);
			
			let plane = plane_fit ( points );
			//~ console.log ("plane:", plane);
			let p = this.graph.nodes[A];
			//~ console.log ("p:",p);
			let p0 = plane.projection_3d ( p );
			//~ console.log ("p0:",p0);
			coplanar_forces[A] = new Point3D ( p0.x-p.x, p0.y-p.y, p0.z-p.z );
			//~ console.log ("coplanar_force of node",A,":",coplanar_forces[A].x,coplanar_forces[A].y,coplanar_forces[A].z);
		}
		
		let bb = bounding_box3D ( this.graph.nodes );
		let diameter = distance_squared3D ( bb.min, bb.max );
		diameter = diameter*parameters.max_translation_tollerance;
		
		//~ console.log ("graph diameter",diameter);
		//~ console.log ();
		
		let prev_total_force = this.total_force;
		this.total_force = 0;
		for ( let A in this.graph.nodes )
		{
			//~ console.log ("Node",A);
			//~ console.log ("initial position", this.graph.nodes[A].x, this.graph.nodes[A].y, this.graph.nodes[A].z);
			//~ console.log ("spring force",springs_forces[A].x,springs_forces[A].y,springs_forces[A].x);
			//~ console.log ("electric force",electric_forces[A].x,electric_forces[A].y,electric_forces[A].z);
			//~ console.log ("coplanar force",coplanar_forces[A].x,coplanar_forces[A].y,coplanar_forces[A].z);

			let df = springs_forces[A].scale ( parameters.spring_stiffness ).add ( electric_forces[A].scale ( parameters.repulsion_constant ) );
			df.add ( coplanar_forces[A].scale ( parameters.coplanar_ETA ) );			
			df.scale ( parameters.ETA_force );
			df.scale ( 1, 1, parameters.anisotropic_coefficient );
			
			let module = distance_squared3D ( df, {x:0, y:0, z:0} );
			//~ console.log ("force module", module);
			if ( module > diameter )
			{
				console.log ("old module:",module);
				let factor = Math.sqrt(diameter/module);
				this.requested_force_factor = Math.min ( this.requested_force_factor, factor );
				df.scale ( factor );
				module = distance_squared3D ( df, {x:0, y:0, z:0} );
				//~ console.log ("Warning: module>diameter");
				console.log ("new module:",module);
			}
			
			
			this.graph.nodes[A].add ( df );
			this.total_force += module;
			//~ console.log ("new position", this.graph.nodes[A].x,this.graph.nodes[A].y,this.graph.nodes[A].z);
			
		}
		//~ console.log ("total force:", this.total_force);
		
		if ( this.requested_force_factor==1 && this.total_force > prev_total_force*parameters.max_force_tollerance )
		{
			this.requested_force_factor = Math.min (parameters.max_force_tollerance, 1/parameters.max_force_tollerance);
		}
		
		return this.graph;
	}
	
	this.update_springs_lengths = function ( new_springs_lengths, alpha )
	{
		for ( let A in new_springs_lengths )
		{
			for ( let B in new_springs_lengths[A] )
			{
				this.springs_lengths[A][B] = alpha*new_springs_lengths[A][B] + (1-alpha)*this.springs_lengths[A][B];
			}
		}
	}
	
	this.reached_stopping_criterion = function ()
	{
		return this.total_force < parameters.force_too_little_to_continue;
	}
	
}
